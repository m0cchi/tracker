FROM alpine:latest

RUN apk --update add git ruby-json exiftool ruby-rmagick imagemagick imagemagick-dev ruby && rm -rf /var/cache/apk/*
RUN cd /root && git clone https://m0cchi@bitbucket.org/m0cchi/tracker.git
RUN echo 'gem: --no-ri --no-rdoc' > /root/.gemrc
RUN gem  install bundler && bundle config --global silence_root_warning 1
RUN cd /root/tracker && bundle install

CMD cd /root/tracker && ruby app.rb -e production
