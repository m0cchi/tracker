require 'bundler'
Bundler.require

def random_id
  o = ('0'..'9').to_a + ('a'..'z').to_a + ('A'..'Z').to_a
  len = (1..4).to_a.sample
  (0..len).map {o.sample}.join
end

def make_img
  image = Magick::Image.new(100,100)
  draw = Magick::Draw.new
  draw.pointsize = 30
  draw.gravity = Magick::CenterGravity

  word = random_id
  draw.annotate(image, 0, 0, 0, 0, word)
  path = "img/#{word}.jpg"
  image.write(path)
  jpg = MiniExiftool.new(path)
  jpg['UserComment']  ="your code: #{word}"
  jpg.save
  path
end


get '/image' do
  response['Access-Control-Allow-Origin'] = '*'
  expires 500, :public, :must_revalidate
  send_file(make_img)
end

get '/id.js' do
  response['Access-Control-Allow-Origin'] = '*'
  expires 500, :public, :must_revalidate
  content_type 'application/javascript'
  <<-EOS
var ID='#{random_id}';
$(window).load(function(){
  var idTag = $('<div/>');
  idTag.text('ID:' + ID);
  $(document.body).append(idTag);
});
EOS
end

get '/' do
  <<-EOS
<!DOCTYPE html>
<html>
  <head>
    <title>index</title>
  </head>
  <body>
    <img id="img1" src="#{ENV['host']}/image">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/exif-js/2.3.0/exif.js"></script>
    <script src="#{ENV['host']}/id.js"></script>
    <script>
    $(window).load(function(){
      var img1 = document.getElementById('img1');
      EXIF.getData(img1, function() {
        var comment = EXIF.getTag(this, 'UserComment');
        var commentTag = $('<div/>');
        var word = String.fromCharCode(...comment)
        commentTag.text(word);
        $(document.body).append(commentTag);
      });
    });
    </script>
  </body>
</html>
EOS
end

